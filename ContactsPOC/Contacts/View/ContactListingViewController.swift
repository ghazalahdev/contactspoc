//
//  ContactListingViewController.swift
//  ContactsPOC
//
//  Created by Ghazalah on 16/05/20.
//  Copyright © 2020 Contacts. All rights reserved.
//

import UIKit
import Contacts
import CoreData

final class ContactListingViewController: UIViewController {
    
    @IBOutlet private var tableView: UITableView!
    
    private let contactSyncService = ContactSyncService(persistentContainer: CoreDataStack.shared.persistentContainer)
    
    private lazy var fetchedResultsController: NSFetchedResultsController<Contact> = {
        let fetchRequest = NSFetchRequest<Contact>(entityName:"Contact")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending:true)]
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                    managedObjectContext: contactSyncService.viewContext,
                                                    sectionNameKeyPath: nil,
                                                    cacheName: nil)
        controller.delegate = self
        
        do {
            try controller.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return controller
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Contacts"
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationRightItems()
        setupAppThemeNavigationBar()
    }

}

extension ContactListingViewController: NavigationBarCustomizable {
    func syncContacts() {
        contactSyncService.fetchAndSyncContacts()
    }
}

extension ContactListingViewController: UITableViewDelegate, UITableViewDataSource {
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: String(describing: ContactTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ContactTableViewCell.self))
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ContactTableViewCell.self), for: indexPath) as? ContactTableViewCell else {
            fatalError("Contact developer of the app, cell for identifier doesn't exists")
        }
        let contact = fetchedResultsController.object(at: indexPath)
        cell.bindData(withContact: contact)
        return cell
    }
}

extension ContactListingViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        tableView.reloadData()
    }
}
