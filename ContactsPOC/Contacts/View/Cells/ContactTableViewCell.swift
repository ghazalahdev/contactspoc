//
//  ContactTableViewCell.swift
//  ContactsPOC
//
//  Created by Ghazalah on 16/05/20.
//  Copyright © 2020 Contacts. All rights reserved.
//

import UIKit

final class ContactTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var emailStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func bindData(withContact contact: Contact) {
        refreshView(withContact: contact)
    }
    
    private func refreshView(withContact contact: Contact) {
        nameLabel.text = contact.name
        phoneNumberLabel.text = contact.phoneNumber
        guard !contact.email.isEmpty else {
            emailStackView.isHidden = true
            return
        }
        emailLabel.text = contact.email
        emailStackView.isHidden = false
    }
    
}
