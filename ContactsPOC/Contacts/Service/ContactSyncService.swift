//
//  ContactSyncService.swift
//  ContactsPOC
//
//  Created by Ghazalah on 16/05/20.
//  Copyright © 2020 Contacts. All rights reserved.
//

import CoreData
import Contacts

struct ContactSyncService {
    private let persistentContainer: NSPersistentContainer
    
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }

    init(persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
    }
        
    func fetchAndSyncContacts() {
        var contacts: [FetchedContact] = []
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { (granted, error) in
            if let error = error {
                print("failed to request access", error)
                return
            }
            if granted {
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                do {
                    try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                        let fetchedContacts = self.getContactsForStoreContact(for: contact)
                        contacts.append(contentsOf: fetchedContacts.compactMap { $0 })
                    })
                    print("CONTACTS_LOG: Total number of contacts on device: \(contacts.count)")
                    self.save(fetchedContacts: contacts)
                } catch let error {
                    print("Failed to enumerate contact", error)
                }
            } else {
                print("access denied")
            }
        }
    }
    
    func save(fetchedContacts contacts: [FetchedContact]) {
        let taskContext = self.persistentContainer.newBackgroundContext()
        taskContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        taskContext.undoManager = nil
        
        sync(fetchedContacts: contacts,
                      taskContext: taskContext)
    }
    
    private func sync(fetchedContacts contacts: [FetchedContact],
                      taskContext: NSManagedObjectContext) {
        taskContext.performAndWait {
            let matchingPhoneNumberRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
            let phoneNumbers = contacts.map { $0.phoneNumber }.compactMap { $0 }
            matchingPhoneNumberRequest.predicate = NSPredicate(format: "phoneNumber in %@", argumentArray: [phoneNumbers])
            do {
                // fetch results from db with phone number in the fetchedContacts.phoneNumber
                let start = CFAbsoluteTimeGetCurrent()
                let fetchResult = try taskContext.fetch(matchingPhoneNumberRequest) as? [Contact]
                let diff = CFAbsoluteTimeGetCurrent() - start
                print("CONTACTS_LOG: Fetching records took: \(diff) seconds")
                let fetchedPhoneNumber = fetchResult?.compactMap({ (contact) -> String? in
                    contact.value(forKey: "phoneNumber") as? String
                })
                var mutableContacts = contacts
                mutableContacts.removeAll(where: { (contact) -> Bool in
                    (fetchedPhoneNumber?.contains(contact.phoneNumber) ?? false)
                })
                let uniqueContacts: Set<FetchedContact> = Set<FetchedContact>(mutableContacts)
                // Create new records.
                for contactData in uniqueContacts {
                    guard let contact = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: taskContext) as? Contact else {
                        print("Error: Failed to create a new Contact object!")
                        return
                    }
                    contact.update(with: contactData)
                }

                let diff1 = CFAbsoluteTimeGetCurrent() - start
                print("CONTACTS_LOG: Operations on fetched contacts took: \(diff1) seconds")
                print("CONTACTS_LOG: contacts non exiting in db but having duplicates: \(mutableContacts.count)")
                print("CONTACTS_LOG: unique contacts to be saved: \(uniqueContacts.count)")
                // Save all the changes just made and reset the taskContext to free the cache.
                if taskContext.hasChanges {
                    do {
                        try taskContext.save()
                        let diff = CFAbsoluteTimeGetCurrent() - start
                        print("CONTACTS_LOG: Total time taken: \(diff) seconds")
                    } catch {
                        print("Error: \(error)\nCould not save Core Data context.")
                    }
                    taskContext.reset() // Reset the context to clean up the cache and low the memory footprint.
                }
            } catch {
                print("Some error")
            }
        }
    }
    
    private func getContactsForStoreContact(for contact: CNContact) -> [FetchedContact] {
        var fetchedContacts: [FetchedContact] = []
        for number in contact.phoneNumbers {
            guard isvalidPhoneNumber(number: number.value.stringValue) else {
                continue
            }
            fetchedContacts.append(FetchedContact(firstName: contact.givenName,
                                                lastName: contact.familyName,
                                                phoneNumber: number.value.stringValue,
                                                email: contact.emailAddresses.first?.value as String? ?? ""))
        }
        return fetchedContacts
    }
    
    private func isvalidPhoneNumber(number: String) -> Bool {
        let landlineRegex = "^[0-9]\\d{2,4}-\\d{6,8}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", landlineRegex)
        let result = phoneTest.evaluate(with: number)
        return !result
    }
}

