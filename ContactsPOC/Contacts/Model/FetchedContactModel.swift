//
//  FetchedContactModel.swift
//  ContactsPOC
//
//  Created by Ghazalah on 16/05/20.
//  Copyright © 2020 Contacts. All rights reserved.
//

import Foundation

struct FetchedContact: Equatable, Hashable {
    var firstName: String
    var lastName: String
    var phoneNumber: String
    var email: String
    
    var name: String {
        return lastName.isEmpty ? firstName : firstName + " " + lastName
    }
}
