//
//  ContactManagedObject.swift
//  ContactsPOC
//
//  Created by Ghazalah on 16/05/20.
//  Copyright © 2020 Contacts. All rights reserved.
//

import CoreData

final class Contact: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var email: String
    @NSManaged var phoneNumber: String

    func update(with fetchedContact: FetchedContact) {
        self.name = fetchedContact.name
        self.email = fetchedContact.email
        self.phoneNumber = fetchedContact.phoneNumber
    }

}
