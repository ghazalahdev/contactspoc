//
//  NavigationBarUtility.swift
//  Errand Junkie
//
//  Created by Ghazalah on 16/05/20.
//  Copyright © 2020 Contacts. All rights reserved.
//

import UIKit

protocol NavigationBarCustomizable: NavigationBarActionable {
    func setupNavigationRightItems()
    func setupAppThemeNavigationBar()
}

@objc protocol NavigationBarActionable: class {
    @objc func syncContacts()
}

extension NavigationBarCustomizable where Self: UIViewController {
    func setupNavigationRightItems() {
        
        let syncContactsBarButton = UIBarButtonItem(title: "Sync", style: .plain, target: self, action: #selector(syncContacts))
        
        self.navigationItem.rightBarButtonItem = syncContactsBarButton
    }
    
    func setupAppThemeNavigationBar() {
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        navigationController?.navigationBar.isTranslucent = false

        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
}
